using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.Rendering;
using UnityEngine;
using UnityEngine.AI;

public class Mover : MonoBehaviour
{
    [SerializeField]
    private Animator characterAnimator;

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            MoveToCursor();
        }
    }

    private void MoveToCursor()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;
        bool hasHit = Physics.Raycast(ray, out hit);

        if(hasHit)
        {
            this.GetComponent<NavMeshAgent>().destination = hit.point;
        }

        Vector3 globalVelocity = this.GetComponent<NavMeshAgent>().velocity;

        // InverseTransformDirection converts global values to local values.
        // Animator only cares about the local Z coordinate value in this case.
        Vector3 localVelocity = transform.InverseTransformDirection(globalVelocity);

        float speed = localVelocity.z;

        characterAnimator.SetFloat("forwardSpeed", speed);
    }
}